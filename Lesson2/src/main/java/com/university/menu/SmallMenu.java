package com.university.menu;

import com.university.common.image.ImageUrl;

public class SmallMenu {
    private ImageUrl main;
    private ImageUrl trends;
    private ImageUrl subscribs;
    private ImageUrl library;

    public ImageUrl getMain() {
        return main;
    }

    public void setMain(ImageUrl main) {
        this.main = main;
    }

    public ImageUrl getTrends() {
        return trends;
    }

    public void setTrends(ImageUrl trends) {
        this.trends = trends;
    }

    public ImageUrl getSubscribs() {
        return subscribs;
    }

    public void setSubscribs(ImageUrl subscribs) {
        this.subscribs = subscribs;
    }

    public ImageUrl getLibrary() {
        return library;
    }

    public void setLibrary(ImageUrl library) {
        this.library = library;
    }
}
