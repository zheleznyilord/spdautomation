package com.university.menu;

import com.university.common.image.ImageUrl;

import java.util.List;

public class BigMenu extends SmallMenu {
    private List<ImageUrl> otherUrls;

    public List<ImageUrl> getOtherUrls() {
        return otherUrls;
    }

    public void setOtherUrls(List<ImageUrl> otherUrls) {
        this.otherUrls = otherUrls;
    }
}
