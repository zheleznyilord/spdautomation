package com.university.common.video;

import com.university.common.Url;
import com.university.common.video.Video;

import java.time.LocalDateTime;

public class VideoUrl extends Url {
    private Video video;
    private int countWatches;
    private String channel;
    private LocalDateTime dateTime;

    public int getCountWatches() {
        return countWatches;
    }

    public void setCountWatches(int countWatches) {
        this.countWatches = countWatches;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
