package com.university.common.norification;

import com.university.common.image.ImageUrl;

import java.time.LocalDateTime;

public class NotificationUrl extends ImageUrl {
    private LocalDateTime notifTime;

    public LocalDateTime getNotifTime() {
        return notifTime;
    }

    public void setNotifTime(LocalDateTime notifTime) {
        this.notifTime = notifTime;
    }
}
