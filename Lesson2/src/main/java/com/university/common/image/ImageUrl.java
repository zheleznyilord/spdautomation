package com.university.common.image;

import com.university.common.Url;
import com.university.common.image.Image;

public class ImageUrl extends Url {
    private Image image;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
