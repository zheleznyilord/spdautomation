package com.university.mainfield;

import com.university.common.video.VideoUrl;

import java.util.List;

public class MainField {
    private List<VideoUrl> recommends;
    private List<VideoUrl> newVideos;
    private List<VideoUrl> otherVideos;

    public List<VideoUrl> getRecommends() {
        return recommends;
    }

    public void setRecommends(List<VideoUrl> recommends) {
        this.recommends = recommends;
    }

    public List<VideoUrl> getNewVideos() {
        return newVideos;
    }

    public void setNewVideos(List<VideoUrl> newVideos) {
        this.newVideos = newVideos;
    }

    public List<VideoUrl> getOtherVideos() {
        return otherVideos;
    }

    public void setOtherVideos(List<VideoUrl> otherVideos) {
        this.otherVideos = otherVideos;
    }
}
