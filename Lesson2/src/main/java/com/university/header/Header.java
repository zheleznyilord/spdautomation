package com.university.header;

import com.university.common.Switch;
import com.university.common.Url;
import com.university.common.image.ImageUrl;

public class Header {
    private Switch menu;
    private Url youTube;
    private YouTubeSearch search;
    private ImageUrl addVideo;
    private AppsMenu AppsMenu;
    private NotificationsMenu notifMenu;
    private Url profile;


    public Switch getMenu() {
        return menu;
    }

    public void setMenu(Switch menu) {
        this.menu = menu;
    }

    public Url getYouTube() {
        return youTube;
    }

    public void setYouTube(Url youTube) {
        this.youTube = youTube;
    }

    public YouTubeSearch getSearch() {
        return search;
    }

    public void setSearch(YouTubeSearch search) {
        this.search = search;
    }

    public ImageUrl getAddVideo() {
        return addVideo;
    }

    public void setAddVideo(ImageUrl addVideo) {
        this.addVideo = addVideo;
    }

    public com.university.header.AppsMenu getAppsMenu() {
        return AppsMenu;
    }

    public void setAppsMenu(com.university.header.AppsMenu appsMenu) {
        AppsMenu = appsMenu;
    }

    public NotificationsMenu getNotifMenu() {
        return notifMenu;
    }

    public void setNotifMenu(NotificationsMenu notifMenu) {
        this.notifMenu = notifMenu;
    }

    public Url getProfile() {
        return profile;
    }

    public void setProfile(Url profile) {
        this.profile = profile;
    }
}
