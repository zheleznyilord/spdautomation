package com.university.header;

import com.university.common.image.ImageUrl;

import java.util.List;

public class AppsMenu {
    private List<ImageUrl> imageUrls;

    public List<ImageUrl> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<ImageUrl> imageUrls) {
        this.imageUrls = imageUrls;
    }
}
