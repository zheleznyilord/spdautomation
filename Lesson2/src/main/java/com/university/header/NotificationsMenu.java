package com.university.header;

import com.university.common.norification.NotificationUrl;

import java.util.List;

public class NotificationsMenu {
    private List<NotificationUrl> notificationUrls;

    public List<NotificationUrl> getNotificationUrls() {
        return notificationUrls;
    }

    public void setNotificationUrls(List<NotificationUrl> notificationUrls) {
        this.notificationUrls = notificationUrls;
    }
}
