package com.university.spd;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WelcomePopup {
    @FindBy(xpath = "//span[contains(text(),'Dismiss')]/ancestor::button")
    private WebElement dismiss;

    public void closePopup(){
        dismiss.click();
    }

    public void waitDisplayed(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(dismiss));

    }

}
