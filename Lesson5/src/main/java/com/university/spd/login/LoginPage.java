package com.university.spd.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "loginButton")
    private WebElement registerButton;

    @FindBy(xpath = "/html/body/app-root/div/mat-sidenav-container/mat-sidenav-content/app-login/div/mat-card/div[1]")
    private WebElement errorMessage;

    public String login(LoginData data) {
        email.sendKeys(data.getLogin());
        password.sendKeys(data.getPassword());
        registerButton.click();
        return errorMessage.getText().trim();
    }

}
