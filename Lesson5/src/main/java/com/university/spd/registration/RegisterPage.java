package com.university.spd.registration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RegisterPage {

    @FindBy(id = "emailControl")
    private WebElement email;

    @FindBy(id = "passwordControl")
    private WebElement password;

    @FindBy(id = "repeatPasswordControl")
    private WebElement rePassword;

    @FindBy(name = "securityQuestion")
    private WebElement sequrityQuestion;

    @FindBy(tagName = "mat-option")
    private List<WebElement> questions;

    @FindBy(id = "securityAnswerControl")
    private WebElement sequrityAnswer;

    @FindBy(id = "registerButton")
    private WebElement registerButton;

    @FindBy(tagName = "mat-error")
    private WebElement errorMessage;


    public String register(RegistrationData data) {
        email.sendKeys(data.getEmail());
        password.sendKeys(data.getPassword());
        rePassword.sendKeys(data.getRePassword());
        sequrityQuestion.click();
        questions.get(1).click();
        sequrityAnswer.sendKeys(data.getSecurityAnswer());
        registerButton.click();
        return errorMessage.getText().trim();
    }
}
