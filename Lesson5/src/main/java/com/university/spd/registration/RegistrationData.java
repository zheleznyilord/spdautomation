package com.university.spd.registration;

public class RegistrationData {
    private String email;
    private String password;;
    private String rePassword;;
    private String securityAnswer;

    public RegistrationData(String email, String password, String rePassword, String securityAnswer) {
        this.email = email;
        this.password = password;
        this.rePassword = rePassword;
        this.securityAnswer = securityAnswer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }
}
