package com.university.spd;

import com.university.spd.login.LoginData;
import com.university.spd.login.LoginPage;
import com.university.spd.registration.RegisterPage;
import com.university.spd.registration.RegistrationData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class UserTest {
    private WebDriver driver;
    private RegisterPage registerPage;
    private RegistrationData registrationData;
    private WelcomePopup welcomePopup;
    private LoginData loginData;
    private LoginPage loginPage;


    @BeforeMethod
    public void before() {
        System.setProperty("webdriver.chrome.driver",
                "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        registrationData = new RegistrationData("VadimMoskalov@ukr.net", "Qq12345",
                "Qq12345", "Tanya");
        registerPage = PageFactory.initElements(driver, RegisterPage.class);
        welcomePopup = PageFactory.initElements(driver, WelcomePopup.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginData = new LoginData("VadimMoskalov@ukr.net", "Qq12345");
    }

    @AfterMethod
    public void afterTest() {

        driver.quit();
    }


    @Test
    public void shouldRegisterUserSuccess() {
        driver.get("https://juice-shop.herokuapp.com/#/register");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        welcomePopup.waitDisplayed(wait);
        welcomePopup.closePopup();
        registerPage.register(registrationData);
    }

    @Test
    public void shouldFailedWhenNotValidMail() {
        driver.get("https://juice-shop.herokuapp.com/#/register");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        welcomePopup.waitDisplayed(wait);
        welcomePopup.closePopup();
        registrationData.setEmail("Vadim");
        String error = registerPage.register(registrationData);
        Assert.assertEquals(error,
                "Email address is not valid.");
    }

    @Test
    public void shouldFailedWhenPasswordsNotMatch() {
        driver.get("https://juice-shop.herokuapp.com/#/register");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        welcomePopup.waitDisplayed(wait);
        welcomePopup.closePopup();
        registrationData.setRePassword("Qq123");
        String error = registerPage.register(registrationData);

        Assert.assertEquals(error,
                "Passwords do not match");
    }


    @Test
    public void shouldLoginFailed() {
        driver.get("https://juice-shop.herokuapp.com/#/login");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        welcomePopup.waitDisplayed(wait);
        welcomePopup.closePopup();
        loginData.setPassword("qwesdgwweeq");
        String error = loginPage.login(loginData);

        Assert.assertEquals(error,
                "Invalid email or password.");
    }

    @Test
    public void shouldLoginSuccess() {
        driver.get("https://juice-shop.herokuapp.com/#/login");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        welcomePopup.waitDisplayed(wait);
        welcomePopup.closePopup();
        String error = loginPage.login(loginData);

        Assert.assertEquals(driver.getCurrentUrl(), "https://juice-shop.herokuapp.com/#/search");
    }
}
