package calculator;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CalculatorTest {
    private  Calculator calculator;

    @BeforeMethod
    public void beforeTest(){
        calculator = new Calculator();
    }


    @Test
    public void testAdd(){
        Result actualForOneAndTwo = calculator.add(1,2);
        Result actualForTwoAndOne = calculator.add(2,1);
        Result actualForThreeAndZero = calculator.add(3,0);
        Result actualForOneAndFor = calculator.add(-1,4);
        Result actualForSevenAndTen = calculator.add(-7, 10);
        int expected = 3;

        SoftAssert asert=new SoftAssert();

        asert.assertEquals( expected, (int)actualForOneAndTwo.getResult(),"actual result != "+ expected);
        asert.assertEquals( expected, (int)actualForTwoAndOne.getResult(),"actual result != "+ expected);
        asert.assertEquals( expected, (int)actualForOneAndFor.getResult(),"actual result != "+ expected);
        asert.assertEquals( expected, (int)actualForThreeAndZero.getResult(),"actual result != "+ expected);
        asert.assertEquals( expected, (int)actualForSevenAndTen.getResult(),"actual result != "+ expected);

        asert.assertAll();
    }


    @Test
    public void testAddNonPositiveNumbers(){
        Result actual = calculator.add(-1,-2);
        int expected = -3;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testMinus(){
        Result actual = calculator.minus(3,2);
        int expected = 1;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testMinusWithNonPositive(){
        Result actual = calculator.minus(-3,-2);
        int expected = -1;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testMinusWithOneNonPositive(){
        Result actual = calculator.minus(-3,2);
        int expected = -5;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testMultiply(){
        Result actual = calculator.multiply(-3,-2);
        int expected = 6;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testMultiplyPositive(){
        Result actual = calculator.multiply(4,-2);
        int expected = -8;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testMultiplyZero(){
        Result actual = calculator.multiply(0,2);
        int expected = 0;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testDeletion(){
        Result actual = calculator.deletion(6,2);
        int expected = 3;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testDeletionFloat(){
        Result actual = calculator.deletion( 9,2);
        float expected = 4.5f;

        Assert.assertEquals(new Float(expected),new Float(actual.getResult()),"actual result != "+ expected);
    }

    @Test
    public void testDeletionNonPositive(){
        Result actual = calculator.deletion(-6,2);
        int expected = -3;

        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }

    @Test
    public void testRemainder(){
        Result actual = calculator.remainder(5,2);
        int expected = 1;
        Assert.assertEquals(expected,(int)actual.getResult(),"actual result != "+ expected);
    }
}
