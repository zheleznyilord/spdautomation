import calculator.Calculator;

public class Main {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        System.out.println(calculator.add(1,2).getResult());
        System.out.println(calculator.minus(3,1).getResult());
        System.out.println(calculator.multiply(2,2).getResult());
        System.out.println(calculator.deletion(7,0).getResult());
        System.out.println(calculator.remainder(2,0).getResult());
    }
}
