package calculator;

public class Calculator {


    public Result add(int a,int b){
        return new Result(a,b,a+b, Status.SUCCESS);
    }

    public Result minus(int a,int b){
        return new Result(a,b,a-b, Status.SUCCESS);
    }

    public Result multiply(int a,int b){
        return new Result(a,b,a*b, Status.SUCCESS);
    }

    public Result deletion(int a, int b){
        return new Result(a,b,(float) a/b, Status.SUCCESS);
    }

    public Result remainder(int a, int b){
        if (b == 0){
            return new Result(a,b,0,Status.SOMETHING_WENT_WRONG);
        }
        return new Result(a,b,a%b, Status.SUCCESS);
    }


}
