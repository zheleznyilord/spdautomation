package calculator;

public class Result {
    private float FirstValue;
    private float SecondValue;
    private float Result;
    private Status status;

    public Result(float firstValue, float secondValue, float result, Status status) {
        FirstValue = firstValue;
        SecondValue = secondValue;
        Result = result;
        this.status = status;
    }

    public float getFirstValue() {
        return FirstValue;
    }

    public void setFirstValue(float firstValue) {
        FirstValue = firstValue;
    }

    public float getSecondValue() {
        return SecondValue;
    }

    public void setSecondValue(float secondValue) {
        SecondValue = secondValue;
    }

    public float getResult() {
        return Result;
    }

    public void setResult(float result) {
        Result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
