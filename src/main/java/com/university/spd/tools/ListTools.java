package com.university.spd.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ListTools {
    private List<Integer> values;

    public ListTools() {
    }

    public ListTools(List<Integer> values) {
        this.values = values;
    }

    public List<Integer> getValues() {
        return values;
    }

    public void setValues(List<Integer> values) {
        this.values = values;
    }

    public void printCollection(){
        values.forEach(v -> {
            if (values.indexOf(v)!= values.size() - 1) {
                System.out.print(v + ", ");
            } else {
                System.out.print(v);
            }
        });
    }

    public void changeCollection(){
        Optional<Integer> max = values.stream().max(Integer::compareTo);
        Optional<Integer> min = values.stream().min(Integer::compareTo);
        Collections.swap(values,values.indexOf(max.get()),values.indexOf(min.get()));
        printCollection();
    }

    public void generateCollection(int size){
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            integers.add((int) (Math.random() * 10));
        }
        values = integers;
    }

    public boolean distinctAndCheckCount(){
        values = values.stream().distinct().collect(Collectors.toList());
        if (values.size()<4){
            return false;
        }
        return true;
    }

}
