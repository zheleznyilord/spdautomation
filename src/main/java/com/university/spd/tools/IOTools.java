package com.university.spd.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IOTools {
    private static final String PATH = "src/main/resources/demo.csv";

    public List<Integer> readListFromCsv() {
        if (Files.exists(Paths.get(PATH).toAbsolutePath())) {
            try (Stream<String> lines = Files.lines(Paths.get(PATH).toAbsolutePath())) {
                String[] split = lines.findFirst().orElse("").split("\\|");
                List<String> strings = Arrays.asList(split);
                if(strings.contains("")){
                    return Collections.emptyList();
                }
                return strings.stream().map(String::trim).map(Integer::valueOf).collect(Collectors.toList());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Files.createFile(Paths.get(PATH).toAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<Integer> addFromConsole(List<Integer> ints) {
        System.out.println("Your list need a number, please input number:");
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);
        String next = scanner.next();
        ints.add(Integer.valueOf(next));
        StringBuilder result = new StringBuilder("");
        ints.forEach(integer -> {result.append(integer); result.append(" | ");});
        try {
            Files.write(Paths.get(PATH).toAbsolutePath(),
                    result.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ints;
    }
}
