package com.university.spd;

import com.university.spd.tools.IOTools;
import com.university.spd.tools.ListTools;


public class Main {

    public static void main(String[] args) {
        ListTools tools = new ListTools();
        tools.generateCollection(10);
        tools.printCollection();
        System.out.println();
        tools.changeCollection();
        IOTools ioTools = new IOTools();
        tools.setValues(ioTools.readListFromCsv());
        while (!tools.distinctAndCheckCount()){
            tools.setValues(ioTools.addFromConsole(tools.getValues()));
        }
        System.out.println();
        tools.printCollection();

    }
}
