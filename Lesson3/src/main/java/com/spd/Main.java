package com.spd;

import com.spd.common.Button;
import com.spd.common.CheckBox;
import com.spd.common.TextField;
import com.spd.common.Url;
import com.spd.loginPage.LoginForm;
import com.spd.loginPage.LoginPage;
import com.spd.user.User;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        LoginPage loginPage = new LoginPage();
        LoginForm loginForm = new LoginForm();
        loginForm.setRememberMe(new CheckBox(false,"Remember me"));
        loginForm.setForgotPassword(new Url());
        loginForm.setLoginWithGoogle(new Button());
        loginForm.setRegister(new Url());
        loginForm.setButton(new Button(220,221,false,"login","green"));
        loginForm.setTittle("LoginForm");
        ArrayList<TextField> fields = new ArrayList<>();
        fields.add(new TextField("login",""));
        fields.add(new TextField("password",""));
        loginForm.setFields(fields);
        loginPage.setForm(loginForm);
        User user = new User("Vadim","123");
        boolean result = loginPage.login(user);
        if (result){
            System.out.println("You are login correct");
        } else {
            System.out.println("something went wrong");
        }
    }


}