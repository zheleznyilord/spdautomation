package com.spd.common;

import java.util.List;

public class Menu {
    private List<ImageUrl> urls;

    public List<ImageUrl> getUrls() {
        return urls;
    }

    public void setUrls(List<ImageUrl> urls) {
        this.urls = urls;
    }
}
