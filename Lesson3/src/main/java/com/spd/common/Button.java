package com.spd.common;

public class Button {
    private int weight;
    private int height;
    private boolean isClicked;
    private String tittle;
    private String color;

    public Button() {
    }

    public Button(int weight, int height, boolean isClicked, String tittle, String color) {
        this.weight = weight;
        this.height = height;
        this.isClicked = isClicked;
        this.tittle = tittle;
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void click(){
        isClicked = true;
        System.out.println(String.format("Button %s  is clicked",tittle));
    }

}
