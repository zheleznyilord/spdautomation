package com.spd.common;

import java.util.List;

public class Form {
    private String tittle;
    private List<TextField> fields;
    private Button button;

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public List<TextField> getFields() {
        return fields;
    }

    public void setFields(List<TextField> fields) {
        this.fields = fields;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
}
