package com.spd.common;

public class CheckBox {
    private boolean isNoted;
    private String tittle;

    public CheckBox(boolean isNoted, String tittle) {
        this.isNoted = isNoted;
        this.tittle = tittle;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public boolean isNoted() {
        return isNoted;
    }

    public void setNoted(boolean noted) {
        isNoted = noted;
    }

    public void click(){
        System.out.println(String.format("CheckBox %s clicked",tittle));
        isNoted = !isNoted;
    }
}
