package com.spd.common;

import java.util.Map;

public class DropBox {
    private Map<String,Boolean> strings;

    public Map<String, Boolean> getStrings() {
        return strings;
    }

    public void setStrings(Map<String, Boolean> strings) {
        this.strings = strings;
    }
}
