package com.spd.loginPage;

import com.spd.common.Button;
import com.spd.common.Form;
import com.spd.common.ImageUrl;
import com.spd.common.TextField;
import com.spd.user.User;

import java.util.List;
import java.util.stream.Collectors;

public class LoginPage {
    private Button menu;
    private ImageUrl main;
    private ImageUrl search;
    private ImageUrl account;
    private ImageUrl language;

    private LoginForm form;

    public LoginPage() {
    }

    public Button getMenu() {
        return menu;
    }

    public void setMenu(Button menu) {
        this.menu = menu;
    }

    public ImageUrl getMain() {
        return main;
    }

    public void setMain(ImageUrl main) {
        this.main = main;
    }

    public ImageUrl getSearch() {
        return search;
    }

    public void setSearch(ImageUrl search) {
        this.search = search;
    }

    public ImageUrl getAccount() {
        return account;
    }

    public void setAccount(ImageUrl account) {
        this.account = account;
    }

    public ImageUrl getLanguage() {
        return language;
    }

    public void setLanguage(ImageUrl language) {
        this.language = language;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(LoginForm form) {
        this.form = form;
    }

    public LoginPage(Button menu, ImageUrl main, ImageUrl search, ImageUrl account, ImageUrl language, LoginForm form) {
        this.menu = menu;
        this.main = main;
        this.search = search;
        this.account = account;
        this.language = language;
        this.form = form;
    }

    public boolean login(User user){
        List<TextField> fields = form.getFields();
        fields.stream()
                .filter(f -> f.getName().equals("login"))
                .forEach(f->f.setText(user.getLogin()));
        fields.stream()
                .filter(f -> f.getName().equals("password"))
                .forEach(f->f.setText(user.getPassword()));
        Button button = form.getButton();
        button.click();
        form.getRememberMe().setNoted(false);
        boolean isLogined = button.isClicked();
        button.setClicked(false);
        return isLogined;
    }
}
