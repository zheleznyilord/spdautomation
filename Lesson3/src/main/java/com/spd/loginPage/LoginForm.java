package com.spd.loginPage;

import com.spd.common.*;

public class LoginForm extends Form {
    private CheckBox rememberMe;
    private Button loginWithGoogle;
    private Url forgotPassword;
    private Url register;

    public CheckBox getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(CheckBox rememberMe) {
        this.rememberMe = rememberMe;
    }

    public Button getLoginWithGoogle() {
        return loginWithGoogle;
    }

    public void setLoginWithGoogle(Button loginWithGoogle) {
        this.loginWithGoogle = loginWithGoogle;
    }

    public Url getForgotPassword() {
        return forgotPassword;
    }

    public void setForgotPassword(Url forgotPassword) {
        this.forgotPassword = forgotPassword;
    }

    public Url getRegister() {
        return register;
    }

    public void setRegister(Url register) {
        this.register = register;
    }
}